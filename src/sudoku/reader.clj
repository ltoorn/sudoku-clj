(ns sudoku.reader
  (:require [sudoku.solver :as solver]
            [clojure.string :as str]))

(def indexes (range 0 81))

(defn zipmap-sudoku [sud]
  (zipmap indexes sud))

(defn associate-possible-values [sudoku-map]
  (into {}
        (map
         (fn [[index value]]
           (if (zero? value)
             [index (set (range 1 10))]
             [index value]))
         sudoku-map)))

(defn process-into-map [sud]
  ((comp associate-possible-values
         (partial zipmap indexes))
    sud))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Functions specific to processing the the input
;; file with 50 Sudokus from the Project Euler
;; problem.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn read-sudokus [path]
  (slurp path))

(defn parse-sudokus [input]
  (let [values (->> input
                    (str/split-lines)
                    (filter #(not (str/includes? % "Grid")))
                    (mapcat vec)
                    (map #(Integer/valueOf (str %))))]
    (loop [sudokus []
           current []
           vals values]
      (cond
        (not (first vals)) (conj sudokus current)
        (= (count current) 81) (recur (conj sudokus current) [(first vals)] (rest vals))
        :else (recur sudokus (conj current (first vals)) (rest vals))))))

(defn process-parsed-input [parsed-input]
  (->> parsed-input
       (map #(zipmap-sudoku %))
       (map #(associate-possible-values %))))

(defn euler-sum [sudokus]
  (->> sudokus
       (pmap (fn [s] (let [sud (solver/solve s)] (+ (* 100 (sud 0)) (* 10 (sud 1)) (sud 2)))))
       (reduce +)))
