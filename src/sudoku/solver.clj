(ns sudoku.solver)

(defn row-nr
  "Returns the row number of `index`. With our 'flat' Sudoku representation, integer division
  of the index by 9 will give us the row number, because the Sudoku grid has nine rows, so every
  nine elements we're on a new row."
  [index] (quot index 9))

(defn column-nr
  "Returns the column number of `index`. With our 'flat' Sudoku representation, the index modulo
  9 will give us the column number. With every element we move up one column, but because the
  Sudoku grid is only nine columns wide, we need to wrap to zero at every 9nth element."
  [index] (mod index 9))

(defn sub-grid-nr
  "Assigns unique sub-grid number to each of the 9 possible
  sub-grids in the Sudoku puzzle. Returns the sub-grid-nr."
  [index] (+
           (quot (row-nr index) 3)
           (* (quot (column-nr index) 3) 10)))

(defn same-row? [x y] (= (row-nr x) (row-nr y)))
(defn same-column? [x y] (= (column-nr x) (column-nr y)))
(defn same-sub-grid? [x y] (= (sub-grid-nr x) (sub-grid-nr y)))

(defn peers-helper? [x y]
  (and
   (not= x y)
   (or (same-row? x y)
       (same-column? x y)
       (same-sub-grid? x y))))

(def peers? (memoize peers-helper?))

(defn extract-solved-cells
  "Returns the cells that are in a solved state. A cell is in a solved state when
  it contains a single numeric value."
  [sud]
  (filter (fn [[_ cell-value]] (number? cell-value)) sud))

(defn unsolved-peers
  "Returns a seq of map entries of only those peers of `cell` that
  contain sets of possible values."
  [sud cell]
  (filter (fn [[k v]] (and (coll? v) (peers? k cell))) sud))

(defn eliminate-excluded-value-from-peers
  "Eliminates the excluded `cell-value` from all of the sets of possible
  values of the peers of `cell` in the `sud`."
  [sud [cell-index cell-value]]
  (let [ps (unsolved-peers sud cell-index)]
    (into sud
          (map
            (fn [[peer-index peer-value]]
              [peer-index (disj peer-value cell-value)])
            ps))))

(defn eliminate-all
  "Eliminates all excluded values from the sets of possible values."
  [sud]
  (loop [cells (extract-solved-cells sud)
         sudoku-state sud]
    (if-let [cell (first cells)]
      (recur (rest cells) (eliminate-excluded-value-from-peers sudoku-state cell))
      sudoku-state)))

(defn assign-singleton-sets
  "Assigns all known values in the Sudoku from singleton sets.
  Returns `nil` if there are no known values to assign."
  [sud]
  (let [known-values (filter (fn [[_ cell-value]] (and (coll? cell-value) (= (count cell-value) 1))) sud)]
    (if (seq known-values)
      (into sud
            (map (fn [[cell-index cell-value]] [cell-index (first cell-value)]) known-values))
      nil)))

(defn propagate
  "Propagates the constraints (each value may only occur once on its
  row, column, and sub grid) through the Sudoku."
  [sud]
  (let [sudoku-state (eliminate-all sud)]
    (if-let [updated-state (assign-singleton-sets sudoku-state)]
      (recur updated-state)
      sudoku-state)))

(defn invalid-cell?
  "Determines whether the cell with `cell-index` and `cell-value` is invalid. A cell is
  in an invalid state when one of its peers has the same value."
  [sud [cell-index cell-value]]
  (let [solved-peers-set (->> sud
                   ;; First filter out all peers that are in a solved state.
                   (filter
                     (fn [[potential-peer-index potential-peer-value]]
                       (and (number? potential-peer-value) (peers? potential-peer-index cell-index))))
                   ;; Then reduce the collected peer values to a set so that we can easily test against it.
                   (reduce (fn [result [_ peer-value]] (conj result peer-value)) #{}))]
    (contains? solved-peers-set cell-value)))

(defn illegal-state?
  "Determines whether the Sudoku grid `sud` is in an illegal state. This is the case when
  there are any two peers that are both in a solved state and that both have the same value."
  [sud]
  (loop [solved-cells (extract-solved-cells sud)]
    ;; If there are any cells that are in a solved state, we'll check their validity.
    (if-let [cell (first solved-cells)]
      (if (invalid-cell? sud cell)
        true
        (recur (rest solved-cells)))
      false)))

(defn determine-cell-for-guess
  "Returns the unsolved cell with the least amount of possible values in `sud`,
  and `nil` if no such cell exists (i.e. all cells are in a solved state)."
  [sud]
  (first
    (sort-by
      (fn [[_ cell-values]] (count cell-values)) ;; Sort by the number of possible solutions for the given cell.
      (filter
        (fn [[_ cell-value]] (coll? cell-value)) ;; We're only interested in unsolved cells.
        sud))))

(defn solve
  "Solves the Sudoku `sud` by constraint propagation, and backtracking when necessary."
  [sud]
  (let [current-sudoku-state (propagate sud)
        [index values-for-guess :as unsolved-cell] (determine-cell-for-guess current-sudoku-state)]
    (cond
      (illegal-state? current-sudoku-state) nil
      (not unsolved-cell) current-sudoku-state ;; The Sudoku is valid and all values are known.
      :else (loop [guesses values-for-guess]
              ;; Constraint propagation did not lead to a solution. Determine
              ;; a guess and try to solve from the new position.
              (if-let [guess (first guesses)]
                (if-let [solution (solve (assoc current-sudoku-state index guess))]
                  solution
                  (recur (rest guesses)))
                ;; None of the current set of guesses led to a valid solution.
                nil)))))
