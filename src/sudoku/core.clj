(ns sudoku.core
  (:require [sudoku.reader :as r])
  (:gen-class))

(set! *warn-on-reflection* true)

(defn input [path]
  ((comp r/process-parsed-input
         r/parse-sudokus
         r/read-sudokus)
   path))

(defn -main
  [& args]
  (if-let [path (first args)]
    (time (println (r/euler-sum (input path))))
    (println "usage: <path>"))
  (shutdown-agents))
