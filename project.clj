(defproject sudoku "0.1.0-SNAPSHOT"
  :description "Sudoku solver using constraint propagation and backtracking."
  :url "https://gitlab.com/ltoorn/sudoku-clj"
  :license {:name "MIT License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main ^:skip-aot sudoku.core
  :target-path "target/%s"
  :jvm-opts ["-Xms2048m" "-Xmx5g"]
  :uberjar-name "sudoku-solver-standalone.jar"
  :profiles {:uberjar {:aot :all}})
