(ns sudoku.core-test
  (:require [clojure.test :refer :all]
            [sudoku.core :refer :all]
            [sudoku.solver :refer :all]
            [sudoku.reader :refer :all]))

(def test-sudoku
  (process-into-map
    [9 0 1  8 5 0  6 4 0
     0 5 0  0 0 0  1 0 8
     8 0 0  4 9 0  0 0 2

     0 0 0  0 4 5  0 2 0
     0 0 5  0 6 0  4 0 0
     0 9 0  3 8 0  0 0 0

     7 0 0  0 3 4  0 0 5
     3 0 2  0 0 0  0 8 0
     0 1 9  0 7 8  2 0 4]))

(deftest row-test
  (testing "row"
    (is (= (row-nr 5) 0))
    (is (= (row-nr 8) 0))
    (is (= (row-nr 9) 1))
    (is (= (row-nr 80) 8))))

(deftest column-test
  (testing "column"
    (is (= (column-nr 0) 0))
    (is (= (column-nr 1) 1))
    (is (= (column-nr 8) 8))
    (is (= (column-nr 9) 0))))

(deftest sub-grid-test
  (testing "sub-grid"
    (is (= (sub-grid-nr 0) 0))
    (is (= (sub-grid-nr 5) 10))
    (is (= (sub-grid-nr 8) 20))
    (is (= (sub-grid-nr 9) 0))))

(deftest same-sub-grid-test
  (testing "same-sub-grid"
    (is (true? (same-sub-grid? 0 2)))
    (is (true? (same-sub-grid? 0 9)))
    (is (true? (same-sub-grid? 13 4)))
    (is (true? (same-sub-grid? 8 7)))))

(defn complete? [sud]
  (not-any? (fn [[_ v]] (coll? v)) sud))

(deftest solve-test
  (testing "solve"
    (is (not (complete? test-sudoku)))
    (is (complete? (solve test-sudoku)))))
